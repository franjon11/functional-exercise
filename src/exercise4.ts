export const wordCount = (sequence: string): Record<string, number> => {
  
  if( sequence.trim().length == 0) return {}

  const wordsSequence = sequence.split(" ").map(sequence => sequence.toLowerCase());
  return wordCountFor(wordsSequence)
};


const wordCountFor = (wordsSequence: string[]): Record<string, number> => {
  const resultado: Record<string, number> = {};

  for (let index = 0; index < wordsSequence.length; index++) {

    let word = wordsSequence[index]

    if(resultContainsWord(resultado, word)) resultado[word] += 1
    else resultado[word] = 1
  };
  return resultado
}

const resultContainsWord = (resultado: Record<string, number>, word: string): Boolean => {
  return Object.keys(resultado).includes(word);
}
