export const average = (numbers: number[]): number => {
  return numbers.reduce((x,y) => x + y) / numbers.length
};
